FROM openjdk:8u265-jdk

# Download and install packages
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get -y install curl wget unzip ruby-full

RUN mkdir workdir
COPY pipe /workdir

ENTRYPOINT ["/workdir/pipe.sh"]