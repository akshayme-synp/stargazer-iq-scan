#!/bin/bash

source "$(dirname "$0")/utils/preValidation.sh"
source "$(dirname "$0")/utils/stargazerPrescription.sh"
source "$(dirname "$0")/utils/runWorkflowEngineClient.sh"

preValidation;

if [[ "$STAGE" == "IO" ]]; then
    stargazerPrescription;
elif [[ "$STAGE" == "WORKFLOW" ]]; then
    runWorkflowEngineClient;
fi
