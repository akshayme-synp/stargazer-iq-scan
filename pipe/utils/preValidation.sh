function preValidation() {
    STAGE=${STAGE:?'STAGE variable missing.'};
    IO_SERVER_URL=${IO_SERVER_URL:?'IO_SERVER_URL variable missing.'};
    IO_ACCESS_TOKEN=${IO_ACCESS_TOKEN:?'IO_ACCESS_TOKEN variable missing.'};
    WORKFLOW_ENGINE_SERVER_URL=${WORKFLOW_ENGINE_SERVER_URL:?'WORKFLOW_ENGINE_SERVER_URL variable missing.'};
    WORKFLOW_ENGINE_ACCESS_TOKEN=${WORKFLOW_ENGINE_ACCESS_TOKEN:?'WORKFLOW_ENGINE_ACCESS_TOKEN variable missing.'};
    WORKFLOW_ENGINE_VERSION=${WORKFLOW_ENGINE_VERSION:?'WORKFLOW_ENGINE_VERSION variable missing.'};
    APP_MANIFEST_FILE=${APP_MANIFEST_FILE:="ApplicationManifest.yml"};
    IO_MANIFEST_FILE=${IO_MANIFEST_FILE:="SecurityManifest.yml"};
    WORKFLOW_ENGINE_TEMPLATE_FILE=${WORKFLOW_ENGINE_TEMPLATE_FILE:="workflow.yml.template"};
    PERSONA=${PERSONA:="‘developer’"};
    additionalWorkflowArgs=${additionalWorkflowArgs:=""};
}