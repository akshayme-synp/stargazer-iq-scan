function stargazerPrescription() {
    $(`wget https://sigdevsecops.blob.core.windows.net/intelligence-orchestration/${WORKFLOW_ENGINE_VERSION}/bb-prescription.sh`)
    chmod +x bb-prescription.sh
    sed -i -e 's/\r$//' bb-prescription.sh

    # fixme - if something goes wrong here!?
    ./bb-prescription.sh \
        --IO.url=${IO_SERVER_URL} \
        --IO.token=${IO_ACCESS_TOKEN} \
        --bitbucket.username=${BITBUCKET_USERNAME} \
        --bitbucket.password=${BITBUCKET_ACCESS_TOKEN} \
        --app.manifest.path=${APP_MANIFEST_FILE} \
        --sec.manifest.path=${IO_MANIFEST_FILE} \
        --stage=${STAGE} \
        --persona=${PERSONA} \
        ${additionalWorkflowArgs}

    isSastEnabled=$(ruby -rjson -e 'j = JSON.parse(File.read("result.json")); puts j["security"]["activities"]["sast"]["enabled"]')
    isScaEnabled=$(ruby -rjson -e 'j = JSON.parse(File.read("result.json")); puts j["security"]["activities"]["sca"]["enabled"]')
    echo "IS_SAST_ENABLED=${isSastEnabled}" > pipe.meta.env
    echo "IS_SCA_ENABLED=${isScaEnabled}" >> pipe.meta.env
}