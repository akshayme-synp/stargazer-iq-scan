function runWorkflowEngineClient() {
    $(`wget https://sigdevsecops.blob.core.windows.net/intelligence-orchestration/${WORKFLOW_ENGINE_VERSION}/bb-prescription.sh`)
    chmod +x bb-prescription.sh
    sed -i -e 's/\r$//' bb-prescription.sh

    # fixme - if something goes wrong here!?
    ./bb-prescription.sh \
        --IO.url=${IO_SERVER_URL} \
        --IO.token=${IO_ACCESS_TOKEN} \
        --stage=${STAGE} \
        --workflow.template=${WORKFLOW_ENGINE_TEMPLATE_FILE} \
        --polaris.url=${POLARIS_SERVER_URL} \
        --polaris.token=${POLARIS_ACCESS_TOKEN} \
        --blackduck.url=${BLACKDUCK_SERVER_URL} \
        --blackduck.api.token=${BLACKDUCK_ACCESS_TOKEN} \
        --bitbucket.commit.id=${BITBUCKET_COMMIT} \
        --bitbucket.username=${BITBUCKET_USERNAME} \
        --bitbucket.password=${BITBUCKET_ACCESS_TOKEN} \
        --IS_SAST_ENABLED=${IS_SAST_ENABLED} \
        --IS_SCA_ENABLED=${IS_SCA_ENABLED}
    
    echo "Workflow file generated successfullly....Calling WorkFlow Engine"
    $(`wget https://sigdevsecops.blob.core.windows.net/intelligence-orchestration/${WORKFLOW_ENGINE_VERSION}/WorkflowClient.jar`)
    chmod +x WorkflowClient.jar

    # fixme - if something goes wrong here!?
    java -jar WorkflowClient.jar \
        --workflowengine.url=${WORKFLOW_ENGINE_SERVER_URL} \
        --workflowengine.token="${WORKFLOW_ENGINE_ACCESS_TOKEN}" \
        --app.manifest.path=${APP_MANIFEST_FILE} \
        --sec.manifest.path=synopsys-io-workflow.yml
}
